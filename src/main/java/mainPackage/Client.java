package mainPackage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import customException.SameAccountException;

import java.util.Scanner;

import java.util.ArrayList;

public class Client {

	private static final Scanner SCANNER = new Scanner(System.in);
	private ArrayList<String> usernames;
	private ArrayList<String> passwords;
	private ArrayList<Account> allAccountDetails;
	private ArrayList<Account> userAccountDetails;
	private String user_input;

	private String text_file;

	private static Logger logger = LogManager.getLogger(Client.class);

	private static final String pathAccountDetails = "src/main/resources/account details.txt";
	private static final String pathPasswords = "src/main/resources/usernames and passwords.txt";
	public static Client client;
	static {
		try {
			client = new Client();
		} catch (SameAccountException e) {
			System.out.println(e.getMessage());
		} // end try-catch
	} // end static init block

	public Client() throws SameAccountException {

		text_file = "";
		try {
			text_file = new FileRead(pathPasswords).toString();
		} catch (Exception e) {
			logger.error("Path not found");
		}

		String user_data[] = text_file.split(" ");
		// split la text file , unde username va avea index n si parola index n+1
		// despartite prin 'space' , le introduc intr- un vector pt a le accesa

		usernames = new ArrayList<String>();
		passwords = new ArrayList<String>();

		for (int i = 0; i < user_data.length; i += 2) {
			usernames.add(user_data[i]);
			passwords.add(user_data[i + 1]);

		}

	}

	public void logIn() {
		boolean end = false;
		// Sesiunea de log in , afiseaza interogari si menu -ul cu comenzi
		client.setUserAccountDetails(pathAccountDetails);
		while (!"1".equals(user_input)) {

			System.out.println("Introduceti login : \nTastati 1 pentru a iesi");
			user_input = SCANNER.nextLine();

			for (int i = 0; i < usernames.size(); i++) {

				if (user_input.equals(usernames.get(i))) {

					System.out.println("Introduceti parola : \nTastati '1' pentru a inchide programul");
					user_input = SCANNER.nextLine();
					if (user_input.equals(passwords.get(i))) {
						System.out.println("Bine ai venit " + usernames.get(i) + " !");
						getUserAccountDetails(usernames.get(i));
						do {

							System.out.println("Tasteaza '2' pentru log out");
							System.out.println("Tasteaza '3' pentru a afisa conturile");
							System.out.println("Tasteaza '4' pentru a crea un cont nou");
							System.out.println("Tasteaza '5' pentru a face un transfer intre conturile personale");
							user_input = SCANNER.nextLine();

							if ((user_input.equals("2"))) {
								end = true;
							}
							if (user_input.equals("3")) {
								showAccounts(usernames.get(i));
							}
							if (user_input.equals("4")) {
								createAccount(usernames.get(i));
							}
							if (user_input.equals("5")) {
								transferMoney(usernames.get(i));
							}

						} while (!end);

					}
				}
			}

			if (user_input.equals("2")) {
				System.out.println("Logging out ...");
				continue;
			}
			if (user_input.equals("1")) {
				System.out.println("Inchidere program ...");
			} else
				System.out.println("Login / parola gresita !");
		}

	}

	public void setUserAccountDetails(String pathAccountDetails) {

		String text_file = "";
		// Citeste account details.txt si salveaza datele in memorie

		try {
			text_file = new FileRead(pathAccountDetails).toString();
		} catch (Exception e) {
			logger.error("File not found");
		}
		String data_field[] = text_file.split(" ");
		int nr_campuri = 4; // 4 campuri pentru - acc_number, username,balance,acc_type
		allAccountDetails = new ArrayList<Account>();

		try {
			for (int i = 0; i < data_field.length; i += nr_campuri) {

				float tempBalance = Float.valueOf(data_field[i + 2]);
				allAccountDetails.add(new Account(data_field[i], data_field[i + 1], tempBalance, data_field[i + 3]));
			}
		} catch (Exception e) {
			logger.error("Campuri gresite in text file");
		}
	}

	public ArrayList<Account> getUserAccountDetails(String user_name) {
		// salveaza conturile unui user specific in lista userAccountDetails
		// variabila match fixeaza daca a fost gasit vreun cont care apartine user- ului

		userAccountDetails = new ArrayList<Account>();
		boolean match = false;
		for (int i = 0; i < allAccountDetails.size(); i++) {

			if (user_name.equals(allAccountDetails.get(i).getUserName())) {
				userAccountDetails.add(allAccountDetails.get(i));
				match = true;

			}
		}
		if (!match) {
			userAccountDetails.add(new Account("", "", 0f, ""));
		}
		return userAccountDetails;

	}

	public void showAccounts(String userName) {
		// Metoda care arata Numarul contului , suma disponibila

		if (userAccountDetails.get(0).getUserName().equals(""))
			System.out.println("Nu ai deschis nici un cont");
		else {
			for (int index = 0; index < userAccountDetails.size(); index++)

				System.out.println(
						"Numar cont: " + index + "\nNume cont: " + userAccountDetails.get(index).getAccountNumber()
								+ "\nSuma disponibila: " + userAccountDetails.get(index).getBalance()
								+ "\nTipul contului: " + userAccountDetails.get(index).getAccountType() + "\n");
		}
	}

	public ArrayList<Account> createAccount(String user_name) {
		// Creeaza account si salveaza detaliile in fisierul account details.txt prin
		// clasa WriteFile
		// Datele introduse de user sunt validate utilizand regex
		Account tempAccount = new Account(null, user_name, null, null);
		try {

			System.out.println("Introduceti tipul contului RON/EUR");
			tempAccount.setAccountType(SCANNER.next("RON|EUR"));
			System.out.println("Introduceti numarul contului de forma 80BHTG000000123456344 (21 caractere)");
			String temp_acc_number = SCANNER.next("[0-9]{2}[A-Z]{4}[0-9]{15}");
			tempAccount.setAccountNumber(tempAccount.getAccountType() + temp_acc_number);
			System.out.println("Transferati suma ");
			tempAccount.setBalance(SCANNER.nextFloat());

			WriteFile save = new WriteFile(pathAccountDetails, tempAccount.toString());
			userAccountDetails.add(tempAccount);

		} catch (Exception e) {
			logger.error("Eroare la introducerea tipului.");

		}
		return userAccountDetails;
	}

	public ArrayList<Account> transferMoney(String userName) {
		// Transfer de bani , verifica daca clientul are nr de conturi necesare , daca
		// tipurile conturilor coincid , daca nu transfera pe acelasi cont,
		// daca ajung bani

		if (userAccountDetails.size() > 1) { // are rost de facut transfer daca userul are 2 sau mai multe conturi
			try {
				int numarContSursa;
				int numarContDestinatie;
				float suma;

				System.out.println("Alegeti contul sursa: ");
				numarContSursa = SCANNER.nextInt();

				System.out.println("Alegeti contul destinatie: ");
				numarContDestinatie = SCANNER.nextInt();

				if (numarContSursa == numarContDestinatie) {
					throw new SameAccountException("Numarul conturilor nu trebuie sa coincida");
				}
				if (userAccountDetails.get(numarContSursa).getAccountType()
						.equals(userAccountDetails.get(numarContDestinatie).getAccountType())) {

					System.out.println("Introduceti suma: ");
					suma = SCANNER.nextFloat();
					userAccountDetails.get(numarContSursa).balanceSubstract(suma);

					if (userAccountDetails.get(numarContSursa).getBalance() >= 0) {

						System.out.println("Operatiune reusita");
						userAccountDetails.get(numarContDestinatie).balanceAdd(suma);
						return userAccountDetails;
					} else {
						System.out.println("Bani insuficienti !\n");
					}
				} else {

					System.out.println("Tipurile conturilor trebuie sa coincida !\n");
				}

			} catch (ArrayIndexOutOfBoundsException exception) {
				logger.error("Index out of bounds" + exception.getMessage(), exception);
			} catch (SameAccountException e) {
				System.out.println(e.getMessage());
			}

			catch (Exception exc) {
				logger.error("Tip input gresit" + exc.getMessage(), exc);
			}

		} else {
			System.out.println("Utilizatorul nu are nici un cont sau are doar unul\n");
		}
		return userAccountDetails;

	}

	public void getInstance() {

		client.logIn();
	}
}