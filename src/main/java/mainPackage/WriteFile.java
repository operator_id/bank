package mainPackage;
//Clasa care scrie intr-un file
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class WriteFile {
	private static Logger logger = LogManager.getLogger(Client.class);
	public WriteFile(String path,String text ) {
	
		File anotherFile = new File(path);
			if (anotherFile.exists()) {    // If file exists , the program will append text to it
				//System.out.println("The file with this name already exists! \nWriting into existing file...");
				try {
					FileWriter fW= new FileWriter (anotherFile,true);
					BufferedWriter bW= new BufferedWriter(fW);
					bW.write(text);
					bW.close();
					logger.info("Operatiune salvata");
				}
				catch (Exception e ) {
						e.printStackTrace();
				}	
			}
			else {   // if it doesn't , a new file will be created
				//System.out.println("File not found. Creating new file...");
				try {
					FileWriter fW= new FileWriter (anotherFile);
					BufferedWriter bW= new BufferedWriter(fW);
					bW.write(text);
					bW.close();
					logger.info("Success!");
				}
				catch (Exception e ) {
					logger.error("Invalid path");
				}	
				try {			
				anotherFile.createNewFile();						
				}
				catch (Exception e) {
					logger.error("Invalid path");
				}
			}
		}	
	}
	

