package mainPackage;

public class Account {

	private String accountNumber;
	private String userName;
	private Float balance;
	private String accountType;

	public Account(String accountNumber, String userName, Float balance, String accountType) {

		this.accountNumber = accountNumber;
		this.userName = userName;
		this.balance = balance;
		this.accountType = accountType;

	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (accountType == null) {
			if (other.accountType != null)
				return false;
		}
		if (!accountType.equals(other.accountType))
			return false;

		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	public Float getBalance() {
		return balance;
	}

	public void setBalance(Float balance) {
		this.balance = balance;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void balanceSubstract(Float value) {
		this.balance -= value;
	}

	public void balanceAdd(Float value) {
		this.balance += value;
	}

	public String toString() {
		return "\n" + accountNumber + " " + userName + " " + balance + " " + accountType;
	}
}
