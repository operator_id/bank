package mainPackage;
import java.io.BufferedReader;
import java.io.FileReader;
//
//Clasa care citeste file
public class FileRead {
	String text = "";

	public FileRead(String path) throws Exception {

		FileReader file = new FileReader(path);
		BufferedReader read = new BufferedReader(file);
		String line = read.readLine();

		while (line != null) {
			text = text + line + " ";
			line = read.readLine();
		}
		read.close();

	}

	public String toString() {
		return text;
	}
}
